== Summary

=== I still have time left!

If you still have some time left in this lab (and you have some time free
because we didn't want you to run out of time) then here's some extras you
could do;

. Make use of the lab assistants to ask your Azure questions! We use Azure a
lot, and one of us will hopefully know the answer!
. Try automating some of these steps - as they would be necessary when new
releases of RHEL come out, like 8.1, 8.2, etc. 
. What else do you do for a corporate standard build? Configure auditing,
logging, do you often install a mail server, extra logging configuration? Do that on this image.
. Spend time reading through the recommended links! Really there is a lot of
useful stuff there to learn.

=== What did we learn in this lab?!

In this lab we've run through most of the basic steps needed to create a Red
Hat Enterprise Linux image for Azure. 

. We setup **Image Builder** (composer) and the **Web Console** (cockpit) to
build Azure VHD images. The **Anaconda** installer ran in the background and
created a `disk.vhd` Azure Gold image.
. We also setup virtualization support on a RHEL8 server in order to create a
"goldimage" virtual machine for testing.
. There were read-only instructions on how you would upload this image to Azure
later.

We hope that you are now more familiar with the process, please do review the recommended links below to learn more.


=== What if I want to use this lab after {event}?

Sure! This URL should be public, add it to your bookmarks!

The environment you used at {event} will not be available, but you can
easily create your own RHEL8 workstation and then many of the instructions are
similar.

=== Goodbye, and thanks for your time!

Thanks so much for taking the time to sit in this lab. It's super, mega, uber
important to **fill out the survey in the mobile app**. It's like TripAdvisor for {event}
labs. Give us 5 stars and we're very happy, we get to keep our jobs! Anything less and we're sad :( ...but
welcome your honest feedback!

Thank you for participating in the lab, and enjoy the rest of {event}!

== Appendixes

=== Recommended Links

* Using the Web Console (cockpit); https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8-beta/html/managing_systems_using_the_cockpit_web_interface/
* https://access.redhat.com/articles/2758981[Frequently Asked Questions and Recommended Practices for Microsoft Azure]
* https://access.redhat.com/articles/product-configuration-for-azure#regional-product-availability-1[Azure regional product availability]
* https://azure.microsoft.com/en-us/global-infrastructure/services/["Red Hat Linux" (sic) availability by Azure region]
* https://www.redhat.com/en/technologies/cloud-computing/cloud-access[Overview of Cloud Access]
* Community project; https://weldr.io/lorax/intro.html
* RHEL7 Red Hat Knowledge Base article about uploading a RHEL image to Azure; https://access.redhat.com/articles/uploading-rhel-image-to-azure

[#about-lvm]
=== Logical Volume Manager (LVM) in Azure

If you inspect the root disk of these machines, you'll notice that Logical
Volume Management (LVM) is not being used. LVM is very common in physical
machines and on-premise virtual machines to configure OS and Data disks in
software, rather than in hardware.

While LVM is a useful tool still for Data disks in Azure, the Red Hat
recommendation is **not** to use LVM for Operating System disks at this time
(mount points like /, /bin, /usr, /var, etc). 

. Azure's disk resizing capabilities have no understanding of LVM, and risk
damaging disks if resized.

. If LVM has issues during bootup, it would pause the bootup before SSH becomes
available, rendering the machine impossible to login to. Note that some Azure
regions have early support for serial consoles that may make this limitation
less of an issue in the future. However, it is not yet available in all
regions for all machine types. 

Note that officially,
https://access.redhat.com/articles/2758981#can-logical-volume-management-lvm-be-used-for-rhel-vm-disks-in-microsoft-azure-19[LVM
is supported], but is not recommended.

=== Azure Image Builder VS RHEL Image Builder

Microsoft recently released a new service offering called Azure Image Builder.
This is not the same as Red Hat's RHEL Image Builder. Red Hat does not provide
any support for Azure Image Builder, but does provide support for RHEL Image
Builder.

=== Machine-ID Bug

This lab is affected by a bug in virt-customize to do with regenerating the
machine-id; BZ 1557042 . While the true production workaround for this bug is
obvious (truncate the file), it was thought not to complicate this lab with
this step.
